import static org.junit.Assert.*;

/**
 * Created by h p on 2016/4/12.
 */
public class MyUtilTest {
    public static void main(String[] args) {
        //测试边界情况
        if(MyUtil.percentage2fivegrade(0) != "不及格")
            System.out.println("test failed 1!");
        else if(MyUtil.percentage2fivegrade(60) != "及格")
            System.out.println("test failed 2!");
        else if(MyUtil.percentage2fivegrade(70) != "中等")
            System.out.println("test failed 3!");
        else if(MyUtil.percentage2fivegrade(80) != "良好")
            System.out.println("test failed 4!");
        else if(MyUtil.percentage2fivegrade(90) != "优秀")
            System.out.println("test failed 5!");
        else if(MyUtil.percentage2fivegrade(100) != "优秀")
            System.out.println("test failed 6!");
        else
            System.out.println("test passed!");
    }
}