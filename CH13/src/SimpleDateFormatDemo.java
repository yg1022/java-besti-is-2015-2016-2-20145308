/**
 * Created by h p on 2016/4/16.
 */

import java.text.*;
        import java.util.*;

public class SimpleDateFormatDemo {
    public static void main(String[] args) {
        DateFormat dateFormat = new SimpleDateFormat(
                args.length == 0 ? "MM：dd：yyyy" : args[0]);
        System.out.println(dateFormat.format(new Date()));
    }
}
