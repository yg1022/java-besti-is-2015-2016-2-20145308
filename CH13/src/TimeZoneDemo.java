/**
 * Created by h p on 2016/4/16.
 */
import static java.lang.System.out;
import java.util.TimeZone;

public class TimeZoneDemo {
    public static void main(String[] args) {
        TimeZone timeZone = TimeZone.getDefault();
        out.println(timeZone.getDisplayName());
        out.println("\t时区ID：" + timeZone.getID());
        out.println("\t日光节约时数：" + timeZone.getDSTSavings());
        out.println("\tUTC 偏移毫秒数：" + timeZone.getRawOffset());
    }
}
