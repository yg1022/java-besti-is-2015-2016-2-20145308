/**
 * Created by h p on 2016/4/2.
 */
import java.util.*;

public class WorldCount {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        System.out.print("请输入英文：");
        Set words = tokenSet(console.nextLine());
        System.out.printf("不重复单字有 %d个：%s%n", words.size(), words);
    }

    static Set tokenSet(String line) {
        String[] tokens = line.split(" ");
        return new HashSet(Arrays.asList(tokens));
    }
}
