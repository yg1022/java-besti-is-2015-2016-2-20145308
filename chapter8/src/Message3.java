/**
 * Created by h p on 2016/4/3.
 */
import java.util.*;

public class Message3 {
    public static void main(String[] args) {
        Map<String, String> messages = new TreeMap<>((s1, s2) -> -s1.compareTo(s2));
        messages.put("Justin", "Hello！Justin的信息！");
        messages.put("Monica", "给Monica的悄悄话！");
        messages.put("Irene", "Irene的可爱猫喵喵叫！");
        System.out.println(messages);
    }
}
