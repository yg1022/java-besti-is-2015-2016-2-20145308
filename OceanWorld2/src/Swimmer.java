/**
 * Created by h p on 2016/3/26.
 */
public interface Swimmer {
    public abstract void swim();
}
