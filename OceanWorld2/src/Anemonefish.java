/**
 * Created by h p on 2016/3/26.
 */
public class Anemonefish  implements Swimmer {
    private String name;
    public Anemonefish(String name){
        this.name = name;
    }
    public String getName(){
        return name;
    }

    @Override
    public void swim(){
        System.out.printf("小丑鱼 %s 游泳 %n",name);
    }
}
