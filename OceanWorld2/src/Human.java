/**
 * Created by h p on 2016/3/26.
 */
public class Human implements Swimmer {
    private String name;
    public Human(String name){
        this.name = name;
    }
    public String getName(){
        return name;
    }

    @Override
    public void swim(){
        System.out.printf("人类 %s 潜行 %n",name);
    }
}
