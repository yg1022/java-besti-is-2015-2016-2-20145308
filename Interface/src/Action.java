/**
 * Created by h p on 2016/3/27.
 */
public interface Action {
    public static final int STOP = 0;
    public static final int Right = 1;
    public static final int Left = 2;
    public static final int UP = 3;
    public static final int DOWN = 4;
}
