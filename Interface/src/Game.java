/**
 * Created by h p on 2016/3/27.
 */
import static java.lang.System.out;
public class Game {
public static void main(String[] args){
    play(1);
    play(3);
}


public static void play(int action){
    switch(action){
        case 0:
            out.print("播放停止动画");
            break;
        case 1:
            out.print("播放向右动画");
            break;
        case 2:
            out.print("播放向左动画");
            break;
        case 3:
            out.print("播放向上动画");
            break;
        case 4:
            out.print("播放向下动画");
            break;
        default:
            out.print("不支持此动作");
            break;
    }
}
}
