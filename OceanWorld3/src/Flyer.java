/**
 * Created by h p on 2016/3/26.
 */
public interface Flyer {
    public abstract void fly();
}
