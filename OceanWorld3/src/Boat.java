/**
 * Created by h p on 2016/3/27.
 */
public class Boat implements Swimmer {
    protected String name;

    public Boat(String name){
        this.name = name;
    }

    @Override
    public void swim(){
        System.out.printf("船在水面 %s航行%n",name);
    }
}
