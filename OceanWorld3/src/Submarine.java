/**
 * Created by h p on 2016/3/27.
 */
public class Submarine extends Boat implements Diver{
    public Submarine (String name){
        super(name);
    }

    @Override
    public void dive(){
        System.out.printf("潜水艇 %s 潜行%n",name);
    }
}
