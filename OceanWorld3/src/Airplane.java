/**
 * Created by h p on 2016/3/26.
 */
public class Airplane implements Flyer {
    protected String name;

    public Airplane(String name){
        this.name = name;
    }

    @Override
    public void fly(){
        System.out.printf("飞机 %s在飞%n",name);
    }
}
