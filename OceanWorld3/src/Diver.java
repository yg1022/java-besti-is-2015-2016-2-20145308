/**
 * Created by h p on 2016/3/27.
 */
public interface Diver extends Swimmer {
    public abstract void dive();
}
