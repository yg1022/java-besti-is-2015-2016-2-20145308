/**
 * Created by h p on 2016/3/26.
 */
public class Ocean {
        public static void main(String[] args){
            doSwim(new SwimPlayer("贾斯汀"));
            doSwim(new Submarine("黄色一号"));
            doSwim(new Seaplane("空军零号"));
            doSwim(new FlyingFish("甚平"));
            doSwim(new Boat("捕鱼一号"));
        }

        static void doSwim(Swimmer swimmer){
            swimmer.swim();
        }
}
