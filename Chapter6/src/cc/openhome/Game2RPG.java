package cc.openhome;

/**
 * Created by h p on 2016/3/23.
 */
public class Game2RPG {
    public static void main(String[] args){
        Game1SwordsMan game1swordsMan = new Game1SwordsMan();
        game1swordsMan.setName("Justin");
        game1swordsMan.setLevel(1);
        game1swordsMan.setBlood(200);

        Game1Magician game1Magician = new Game1Magician();
        game1Magician.setName("Monica");
        game1Magician.setLevel(1);
        game1Magician.setBlood(100);

        showBlood(game1swordsMan);
        showBlood(game1Magician);
    }

    static void showBlood (Game1Role game1role){
        System.out.printf("%s 血量 %d%n",game1role.getName(),game1role.getBlood());
    }
}
