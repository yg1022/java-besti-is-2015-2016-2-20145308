package cc.openhome;

/**
 * Created by h p on 2016/3/22.
 */
public class Game1RPG {
    public static void main(String[] args){
        demoGame1SwordsMan();
        demoGame1Magician();
    }

    static void demoGame1SwordsMan(){
        Game1SwordsMan game1swordsMan = new Game1SwordsMan();
        game1swordsMan.setName("Justin");
        game1swordsMan.setLevel(1);
        game1swordsMan.setBlood(200);
        System.out.printf("剑士：(%s,%d,%d)%n",game1swordsMan.getName(),game1swordsMan.getLevel(),game1swordsMan.getBlood());
    }

    static void demoGame1Magician(){
        Game1Magician game1Magician = new Game1Magician();
        game1Magician.setName("Monica");
        game1Magician.setLevel(1);
        game1Magician.setBlood(100);
        System.out.printf("魔法师：(%s,%d,%d)%n",game1Magician.getName(),game1Magician.getLevel(),game1Magician.getBlood());
    }
}
