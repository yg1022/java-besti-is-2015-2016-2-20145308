/**
 * Created by h p on 2016/3/24.
 */
public class Role {
    private String name;
    private int level;
    private int blood;

    public int getBlood() {
        return blood;
    }

    public void setBlood(int blood) {
        this.blood = blood;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void fight() {
        //实际上角色如何进行攻击，只有子类知道，所以子类要重新定义fight（）的实际行为
    }
}
