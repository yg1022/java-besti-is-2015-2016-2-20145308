/**
 * Created by h p on 2016/4/22.
 */
class Student{
    public String name;
    public int age;
    public int id;
}
public class StudentTest {
    public static void main(String[] args){
        Student s = new Student();
        s.name = "ZhangSan";
        s.age = 19;
        s.id = 20095203;

        System.out.println("" +
                "学生信息：姓名：" + s.name + " 年龄：" + s.age + " 学号：" + s.id);
    }
}
