/**
 * Created by h p on 2016/4/24.
 */
import static java.lang.System.out;

public class SplitDemo {
    public static void main(String[] args) {
        // 根据逗号切割
        for(String token : "Justin,Monica,Irene".split(",")) {
            out.println(token);
        }
        // 根据Orz切割
        for(String token : "JustinOrzMonicaOrzIrene".split("Orz")) {
            out.println(token);
        }
        // 根据Tab字符切割
        for(String token : "Justin\tMonica\tIrene".split("\\t")) {
            out.println(token);
        }
    }
}
