/**
 * Created by h p on 2016/4/24.
 */
import static java.lang.System.out;
import java.util.*;
import java.util.regex.*;

public class Regex {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        try {
            out.print("输入规则表示式：");
            String regex = console.nextLine();
            out.print("输入要比较的文字：");
            String text = console.nextLine();
            print(match(regex, text));
        } catch(PatternSyntaxException ex) {
            out.println("规则表示式有误");
            out.println(ex.getMessage());
        }
    }

    private static List<String> match(String regex, String text) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        List<String> matched = new ArrayList<>();
        while(matcher.find()) {
            matched.add(String.format(
                    "从索引 %d 开始到索引 %d 之间找到符合文字 \"%s\"%n",
                    matcher.start(), matcher.end(), matcher.group()));
        }
        return matched;
    }

    private static void print(List<String> matched) {
        if(matched.isEmpty()){
            out.println("找不到符合文字");
        }
        else {
            matched.forEach(out::println);
        }
    }
}
