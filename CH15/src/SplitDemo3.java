/**
 * Created by h p on 2016/4/24.
 */
public class SplitDemo3 {
    public static void main(String[] args) {
        for(String str : "Justin dog Monica doggie Irene".split("\\bdog\\b")) {
            System.out.println(str.trim());
        }
    }
}
