/**
 * Created by h p on 2016/3/26.
 */
import java.util.Arrays;

public class ArrayList {
    private Object[] list;
    private int next;//下一个可存储对象的索引

    public ArrayList(int capacity){
        list = new Object[capacity];
    }

    public ArrayList(){
        this(16);
    }

    public void add(Object o){
        if(next == list.length){
            list = Arrays.copyOf(list,list.length*2);
        }
        list[next++] = o;
    }
    public Object get(int index){
        return list [index];
    }

    public int size(){
        return next;
    }
}
