/**
 * Created by h p on 2016/3/13.
 */
public class BasicRange {
    public static void main(String[] args){
        //byte short int long 范围
        System.out.printf("%d ~ %d%n",Byte.MIN_VALUE,Byte.MAX_VALUE);
        System.out.printf("%d ~ %d%n",Short.MIN_VALUE,Short.MAX_VALUE);
        System.out.printf("%d ~ %d%n",Integer.MIN_VALUE,Integer.MAX_VALUE);
        System.out.printf("%d ~ %d%n",Long.MIN_VALUE,Long.MAX_VALUE);
        //float double精度范围
        System.out.printf("%d ~ %d%n",Float.MIN_EXPONENT,Float.MAX_EXPONENT);
        System.out.printf("%d ~ %d%n",Double.MIN_EXPONENT,Double.MAX_EXPONENT);
        //char 可表示 Unicode 范围
        System.out.printf("%h ~ %h%n",Character.MIN_VALUE,Character.MAX_VALUE);
        //boolean的两个值
        System.out.printf("%b ~ %b%n",Boolean.TRUE,Boolean.FALSE);
    }
}
