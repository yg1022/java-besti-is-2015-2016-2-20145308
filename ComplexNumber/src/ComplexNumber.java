/**
 * Created by h p on 2016/4/12.
 */
import java.util.Scanner;
import java.lang.String;

public class ComplexNumber {
    public double m_dRealPart;//实部
    public double m_dImaginPart;//虚部

    public ComplexNumber(){//构造函数，将实部虚部置0
        double m_dRealPart = 0;
        double m_dImaginPart = 0;
    }
    public ComplexNumber(double r,double I){//构造函数，创建复数对象的同时完成负数的实部虚部的初始化
        this.m_dRealPart = r;
        this.m_dImaginPart = I;
    }
    public double GetRealPart(){//获取实部
       return this.m_dRealPart;
   }
    public double GetImaginaryPart(){//获取虚部
        return this.m_dImaginPart;
    }
    public double SetRealPart(double d){//设置实部
        m_dRealPart = d;
        return m_dRealPart;
    }
    public double SetImaginaryPart(double d){//设置虚部
        m_dImaginPart = d;
        return m_dImaginPart;
    }
    ComplexNumber ComplexAdd(ComplexNumber c){//复数相加
        double real = c.GetRealPart();
        double image = c.GetImaginaryPart();
        double newReal = real + m_dRealPart;
        double newImage = image + m_dImaginPart;
        ComplexNumber Result = new ComplexNumber(newReal,newImage);
        return Result;
    }
   /*ComplexNumber ComplexAdd(double c){//复数相加
        double real = c;
        double newReal = real + m_dRealPart;
        ComplexNumber Result = new ComplexNumber(newReal,0);
        return Result;
    }*/
    ComplexNumber ComplexMinus(ComplexNumber c){//复数相减
        double real = c.GetRealPart();
        double image = c.GetImaginaryPart();
        double newReal = m_dRealPart - real ;
        double newImage = m_dImaginPart - image;
        ComplexNumber Result = new ComplexNumber(newReal,newImage);
        return Result;
    }
   /* ComplexNumber ComplexMinus(double c){//复数相减
        double real = c;
        double newReal = real - m_dRealPart;
        ComplexNumber Result = new ComplexNumber(newReal,0);
        return Result;
    }*/
    ComplexNumber ComplexMulti(ComplexNumber c){//复数相乘
        double real = c.GetRealPart();
        double image = c.GetImaginaryPart();
        double newReal = real * m_dRealPart;
        double newImage = image * m_dImaginPart;
        ComplexNumber Result = new ComplexNumber(newReal,newImage);
        return Result;
    }
    /*ComplexNumber ComplexMulti(double c){//复数相乘
        double real = c;
        double newReal = real * m_dRealPart;
        ComplexNumber Result = new ComplexNumber(newReal,0);
        return Result;
    }*/
    public String toString(double a,double b){//把当前复数对象的实部虚部组合成a+bi的字符串形式
        String c;
        if(b == 0) {c =""+a;}//将double型变量转为String类型
        else if(b < 0){c = a+b+"i";}
        else {c = a+"+"+b +"i";}
        return c;
    }


}
