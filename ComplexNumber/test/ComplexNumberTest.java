﻿import static org.junit.Assert.*;

/**
 * Created by h p on 2016/4/13.
 */
import org.junit.Test;
public class ComplexNumberTest {
        @Test
        public void ComplexNumber(){
            //判断ComplexNumber()函数是否有错
          
	    System.out.printf("已修改（版面）\n");

            assert num1.m_dRealPart == 0 : "fault1:ComplexNumber()函数有错误";
            assert num1.m_dImaginPart == 0 : "fault2:ComplexNumber()函数有错误";

            //判断ComplexNumber(double r,double I)函数是否有错
            ComplexNumber num2 = new ComplexNumber(0, 0);

            assert num2.m_dRealPart == 0 : "fault3:ComplexNumber(double r,double I)函数有错误";
            assert num2.m_dImaginPart == 0 : "fault4:ComplexNumber(double r,double I)函数有错误";

            //判断GetRealPart()函数是否有错
            assert num2.m_dRealPart == 0 : "fault5:GetRealPart()函数有错误";

            //判断GetImaginaryPart()函数是否有错
            assert num2.m_dImaginPart == 0 : "fault6:GetImaginaryPart()函数有错误";

            //判断SetRealPart(double d)函数是否有错
            ComplexNumber num3 = new ComplexNumber();
            num3.SetRealPart(0);
            assert num3.m_dRealPart == 0 : "fault7:SetRealPart(double d)函数有错误";

            //判断SetImaginaryPart(double d)函数是否有错
            num3.SetImaginaryPart(0);
            assert num3.m_dRealPart == 0 : "fault8:SetImaginaryPart(double d)函数有错误";

            //判断ComplexAdd(ComplexNumber c)函数是否有错
            ComplexNumber num4 = new ComplexNumber();
            ComplexNumber num5 = new ComplexNumber(-1, -1);
            ComplexNumber num6 = new ComplexNumber();
            num4 = num1.ComplexAdd(num1);//自身相加，结果为0
            assert num4.m_dRealPart == 0 : "fault9:ComplexAdd(ComplexNumber c)函数有错误";
            assert num4.m_dImaginPart == 0 : "fault10:ComplexAdd(ComplexNumber c)函数有错误";
            num6 = num1.ComplexAdd(num5);//结果为负数
            assert num6.m_dRealPart == -1 : "fault11:ComplexAdd(ComplexNumber c)函数有错误";
            assert num6.m_dImaginPart == -1 : "fault12:ComplexAdd(ComplexNumber c)函数有错误";

            //判断ComplexMinus(ComplexNumber c)函数是否有错
            ComplexNumber num7 = new ComplexNumber();
            ComplexNumber num8 = new ComplexNumber(1, 1);
            ComplexNumber num9 = new ComplexNumber();
            num7 = num1.ComplexMinus(num1);//自身相减，结果为0
            assert num7.m_dRealPart == 0 : "fault13:ComplexMinus(ComplexNumber c)函数有错误";
            assert num7.m_dImaginPart == 0 : "fault14:ComplexMinus(ComplexNumber c)函数有错误";
            num9 = num1.ComplexMinus(num8);//结果为负数
            assert num9.m_dRealPart == -1 : "fault13:ComplexMinus(ComplexNumber c)函数有错误";
            assert num9.m_dImaginPart == -1 : "fault14:ComplexMinus(ComplexNumber c)函数有错误";

            //判断ComplexMulti(ComplexNumber c)函数是否有错
            ComplexNumber num10 = new ComplexNumber();
            ComplexNumber num11 = new ComplexNumber(-1, -1);
            ComplexNumber num12 = new ComplexNumber();
            ComplexNumber num13 = new ComplexNumber();
            ComplexNumber num14 = new ComplexNumber(1, 1);
            num10 = num1.ComplexMulti(num1);//自身相乘，结果为0
            assert num10.m_dRealPart == 0 : "fault15:ComplexMulti(ComplexNumber c)函数有错误";
            assert num10.m_dImaginPart == 0 : "fault16:ComplexMulti(ComplexNumber c)函数有错误";
            num12 = num1.ComplexMulti(num11);//0与复数相乘，结果为0
            assert num12.m_dRealPart == 0 : "fault17:ComplexMulti(ComplexNumber c)函数有错误";
            assert num12.m_dImaginPart == 0 : "fault18:ComplexMulti(ComplexNumber c)函数有错误";
            num13 = num11.ComplexMulti(num14);//正数与复数相乘，结果为负数
            assert num13.m_dRealPart == -1 : "fault19:ComplexMulti(ComplexNumber c)函数有错误";
            assert num13.m_dImaginPart == -1 : "fault20:ComplexMulti(ComplexNumber c)函数有错误";

            //判断toString(double a,double b)函数是否有错
            ComplexNumber num15 = new ComplexNumber(-1, -1);
            ComplexNumber num16 = new ComplexNumber(0, 0);
            assert num15.toString(num15.m_dRealPart, num15.m_dImaginPart).equals( -1-1+"i") : "fault21:toString(double a,double b)函数有错误";//实部虚部全部为复数
            assert num16.toString(num16.m_dRealPart, num16.m_dImaginPart).equals(0) : "fault22e:toString(double a,double b)函数有错误";//实部虚部全部为0

            //成功
            // System.out.println("没有发现程序错误");
        }
    }
